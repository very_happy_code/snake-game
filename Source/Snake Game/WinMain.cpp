#define WIN32_LEAN_AND_MEAN	//Exclude rarely-used stuff from Windows headers

#include <windows.h>
#include <ctime>
#include <cstdlib>
#include <string>

#include "Snake.h"
#include "MyWarningMacro.h"


void RegisterWindowClass(HINSTANCE hInstance);
HWND CreateAndShowWindow(HINSTANCE hInstance, int nCmdShow, int width, int height);
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void OnGameOver();


#define GAME_TIMER_ID 1

const UINT g_unTimerElapseMilliseconds = 500u;	//defines starting game speed (increase while ingame)
const LPCWSTR g_wszWindowClassName = L"GameWindow";
const std::wstring g_wstrWindowBaseTitile = L"Snake: ";
c_SnakeGame* g_pGame;
HWND g_hMainWindow;


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
					  _In_opt_ HINSTANCE hPrevInstance,
					  _In_ LPWSTR    lpCmdLine,
					  _In_ int       nCmdShow)
{
	srand(static_cast< unsigned int >(time(nullptr)));

	g_pGame = new c_SnakeGame(4, &OnGameOver);

	RegisterWindowClass(hInstance);

	int client_width = g_pGame->cn_nFieldWidth * FIELD_CELL_SIZE + FIELD_CELL_BORDER_WIDTH;
	int client_height = g_pGame->cn_nFieldHeight * FIELD_CELL_SIZE + FIELD_CELL_BORDER_WIDTH;
	g_hMainWindow = CreateAndShowWindow(hInstance, nCmdShow, client_width, client_height);

	MSG msg;
	BOOL getMessageResult;

	while ((getMessageResult = GetMessage(&msg, nullptr, 0, 0)) != FALSE) {
		//if error occured:
		if (getMessageResult == -1) {
#pragma MYWARNING("decide what to do here")
			//TODO: handle the error?
			return FALSE;
		}
		else {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	delete g_pGame;

	return msg.wParam;
}


void RegisterWindowClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wc;

	wc.cbSize = sizeof(WNDCLASSEX);

	wc.style			= CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc		= WndProc;
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= 0;
	wc.hInstance		= hInstance;
	wc.hIcon			= NULL;
	wc.hCursor			= NULL;
	wc.hbrBackground	= (HBRUSH)(COLOR_WINDOW + 1);
	wc.lpszMenuName		= NULL;
	wc.lpszClassName	= g_wszWindowClassName;
	wc.hIconSm			= NULL;

	RegisterClassExW(&wc);
}

HWND CreateAndShowWindow(HINSTANCE hInstance, int nCmdShow, int clientWidth, int clientHeight)
{
	DWORD style = WS_CAPTION | WS_MINIMIZEBOX | WS_OVERLAPPED | WS_SYSMENU;
	RECT size_rect{ 0, 0, clientWidth, clientHeight };

	AdjustWindowRect(&size_rect, style, false);	//get window size based on given client size considering window style

	HWND hwnd = CreateWindowW(
		g_wszWindowClassName,
		g_wstrWindowBaseTitile.c_str(),
		style,
		CW_USEDEFAULT, 0,	//position
		(size_rect.right - size_rect.left), (size_rect.bottom - size_rect.top),	//size
		nullptr, nullptr, hInstance, nullptr);

	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);

	return hwnd;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) {
		case WM_CREATE:
			SetTimer(hWnd, GAME_TIMER_ID, g_unTimerElapseMilliseconds, NULL);
			break;

		case WM_KEYDOWN:
			switch (wParam) {
				case 'W':
				case VK_UP:
					g_pGame->Set_SnakeNextDirection(en_MoveDirections::eUp);
					break;

				case 'S':
				case VK_DOWN:
					g_pGame->Set_SnakeNextDirection(en_MoveDirections::eDown);
					break;

				case 'A':
				case VK_LEFT:
					g_pGame->Set_SnakeNextDirection(en_MoveDirections::eLeft);
					break;

				case 'D':
				case VK_RIGHT:
					g_pGame->Set_SnakeNextDirection(en_MoveDirections::eRight);
					break;

				case VK_ESCAPE:
					g_pGame->m_bPause = true;
					auto result = MessageBox(hWnd, L"Exit?", g_wstrWindowBaseTitile.c_str(), MB_YESNO);
					if (result == IDYES) {
						DestroyWindow(hWnd);
					}
					g_pGame->m_bPause = false;
					break;
			}
			break;

		case WM_TIMER:
			{
				float old_speed_factor = g_pGame->Get_GameSpeedFactor();

				g_pGame->DoLogic();

				if (old_speed_factor != g_pGame->Get_GameSpeedFactor()) {	//game speed has changed
					UINT new_time = static_cast< UINT >(g_unTimerElapseMilliseconds / g_pGame->Get_GameSpeedFactor());
					SetTimer(hWnd, GAME_TIMER_ID, new_time, NULL);	//raplace old timer
				}

				//print game score in window header:
				std::wstring titile = g_wstrWindowBaseTitile + L"game score is " + std::to_wstring(g_pGame->Get_GameScore());
				SetWindowText(hWnd, titile.c_str());

				//causes WM_PAINT:
				RECT rect;
				GetClientRect(hWnd, &rect);
				InvalidateRect(hWnd, &rect, true);
			}
			break;

		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				HDC hdc = BeginPaint(hWnd, &ps);
				g_pGame->Draw(hdc);
				EndPaint(hWnd, &ps);
			}
			break;

		case WM_DESTROY:
			KillTimer(hWnd, GAME_TIMER_ID);
			PostQuitMessage(0);
			break;

		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}


//Setup as gameOverCallback in game:
void OnGameOver()
{
	auto result = MessageBox(NULL,
							 L"Game over!\nDo you wish to retry? (or exit otherwise)",
							 g_wstrWindowBaseTitile.c_str(),
							 MB_RETRYCANCEL | MB_ICONQUESTION);
	
	if (result == IDRETRY) {
		g_pGame->Restart();
	}
	else { //IDCANCEL
		DestroyWindow(g_hMainWindow);
	}
}