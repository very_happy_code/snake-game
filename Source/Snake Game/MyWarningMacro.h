/*
* Helpful stuff to write warnings in Visual Studio error list
*	source: http://goodliffe.blogspot.com/2009/07/c-how-to-say-warning-to-visual-studio-c.html
*/
#pragma once

#define STRINGIZE_HELPER(x) #x
#define STRINGIZE(x) STRINGIZE_HELPER(x)

/*
* Usage:
* #pragma MYWARNING(FIXME: Code removed because...)
*/
#define MYWARNING(desc) message(__FILE__ "(" STRINGIZE(__LINE__) ") : Warning: " #desc)