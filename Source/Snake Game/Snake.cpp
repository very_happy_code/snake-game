#include <cstdlib>
#include <cassert>

#include "Snake.h"
#include "MyWarningMacro.h"


#define BACKGROUND_COLOR RGB(200, 200, 200)
#define CELL_BORDER_COLOR RGB(0, 0, 0)
#define WALL_COLOR RGB(170, 45, 45)
#define TARGET_COLOR RGB(255, 210, 0)
#define SNAKE_COLOR RGB(0, 0, 200);
#define SNAKE_PEN_STYLE PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_ROUND | PS_JOIN_ROUND
#define SNAKE_BORDER_OFFSET 2	//measured in pixels or draw units


const float c_SnakeGame::st_cn_fMinGameSpeedFactor = 1.0f;
const float c_SnakeGame::st_cn_fMaxGameSpeedFactor = 3.0f;
const float c_SnakeGame::st_cn_fGameSpeedFactorIncreaseDelta = 0.1f;


c_SnakeGame::c_SnakeGame(int desiredSnakeLength, fp_GameOverCallback_t gameOverCallback)
	:cn_backgroundBrush(CreateSolidBrush(BACKGROUND_COLOR)),
	 cn_wallBrush(CreateSolidBrush(WALL_COLOR)),
	 cn_targetBrush(CreateSolidBrush(TARGET_COLOR)),
	 ppc_gameArea(new en_cellState*[cn_nFieldWidth]),
	 pc_snakeSegments(new std::vector<s_snakeSegment>()),
	 cfp_gameOverCallback(gameOverCallback),
	 cn_nDesiredSnakeLength(desiredSnakeLength)
{
	for (int x = 0; x < cn_nFieldWidth; ++x) {
		this->ppc_gameArea[x] = new en_cellState[cn_nFieldHeight];
	}

	LOGBRUSH snake_brush;
	snake_brush.lbStyle = BS_SOLID;
	snake_brush.lbColor = SNAKE_COLOR;
	snake_brush.lbHatch = 0;

	this->snakePen = ExtCreatePen(SNAKE_PEN_STYLE, FIELD_CELL_SIZE - FIELD_CELL_BORDER_WIDTH - SNAKE_BORDER_OFFSET, &snake_brush, 0, NULL);

	initGameForStart();
}


c_SnakeGame::~c_SnakeGame()
{
	for (int x = 0; x < cn_nFieldWidth; ++x) {
		delete[] ppc_gameArea[x];
	}
	delete[] ppc_gameArea;

	pc_snakeSegments->clear();
	delete pc_snakeSegments;

	DeleteObject(cn_backgroundBrush);
	DeleteObject(cn_wallBrush);
	DeleteObject(snakePen);
}


void c_SnakeGame::Draw(HDC hdc)
{
	RECT rect;

	if (bIsGameOver) {
		//clear background:
		rect = { 0, 0, cn_nFieldWidth * FIELD_CELL_SIZE, cn_nFieldHeight * FIELD_CELL_SIZE };
		FillRect(hdc, &rect, WHITE_BRUSH);

		int half_height = cn_nFieldHeight * FIELD_CELL_SIZE / 2;
		rect = { 0, half_height - 50, cn_nFieldWidth * FIELD_CELL_SIZE, half_height };
		DrawText(hdc, L"Game Over!\0", -1, &rect, DT_CENTER);

		return;
	}


	/* fill game rect: */
	//------------------------------------------------------
	rect = { 0, 0, cn_nFieldWidth * FIELD_CELL_SIZE, cn_nFieldHeight * FIELD_CELL_SIZE };
	FillRect(hdc, &rect, cn_backgroundBrush);
	//------------------------------------------------------


	/* draw cells: */
	//------------------------------------------------------
	HPEN pen = CreatePen(PS_SOLID, FIELD_CELL_BORDER_WIDTH, CELL_BORDER_COLOR);
	auto old_pen = SelectObject(hdc, pen);

	//draw vertical lines:
	for (int x = 0; x <= cn_nFieldWidth; ++x) {
		MoveToEx(hdc, x * FIELD_CELL_SIZE, 0, NULL);
		LineTo(hdc, x * FIELD_CELL_SIZE, cn_nFieldHeight * FIELD_CELL_SIZE);
	}
	//horizontal lines:
	for (int y = 0; y <= cn_nFieldHeight; ++y) {
		MoveToEx(hdc, 0, y * FIELD_CELL_SIZE, NULL);
		LineTo(hdc, cn_nFieldWidth * FIELD_CELL_SIZE, y * FIELD_CELL_SIZE);
	}

	SelectObject(hdc, old_pen);
	DeleteObject(pen);
	//------------------------------------------------------


	/* draw walls & targets: */
	//------------------------------------------------------
	auto prev_brush = SelectObject(hdc, cn_targetBrush);

	for (int x = 0; x < cn_nFieldWidth; ++x) {
		for (int y = 0; y < cn_nFieldHeight; ++y) {
			rect.left = x * FIELD_CELL_SIZE + FIELD_CELL_BORDER_WIDTH;
			rect.top = y * FIELD_CELL_SIZE + FIELD_CELL_BORDER_WIDTH;
			rect.right = rect.left + FIELD_CELL_SIZE - FIELD_CELL_BORDER_WIDTH - (FIELD_CELL_BORDER_WIDTH / 2);
			rect.bottom = rect.top + FIELD_CELL_SIZE - FIELD_CELL_BORDER_WIDTH - (FIELD_CELL_BORDER_WIDTH / 2);

			if (ppc_gameArea[x][y] == en_cellState::eWall) {
				FillRect(hdc, &rect, cn_wallBrush);
			}
			else if (ppc_gameArea[x][y] == en_cellState::eTarget) {
				Ellipse(hdc,
						rect.left - (FIELD_CELL_BORDER_WIDTH / 2),
						rect.top - (FIELD_CELL_BORDER_WIDTH / 2),
						rect.right, rect.bottom);
			}
		}
	}
	SelectObject(hdc, prev_brush);
	//------------------------------------------------------

	//draw snake:
	//------------------------------------------------------
	old_pen = SelectObject(hdc, snakePen);

	int length = pc_snakeSegments->size();
	auto points = new POINT[length];

	for (int i = 0; i < length; ++i) {
		points[i].x = (pc_snakeSegments->at(i).m_nX + 1) * FIELD_CELL_SIZE - FIELD_CELL_SIZE / 2;
		points[i].y = (pc_snakeSegments->at(i).m_nY + 1) * FIELD_CELL_SIZE - FIELD_CELL_SIZE / 2;
	}
	Polyline(hdc, points, length);
	delete[] points;

	SelectObject(hdc, old_pen);
	//------------------------------------------------------


	if (m_bPause) {
		int half_height = cn_nFieldHeight * FIELD_CELL_SIZE / 2;
		rect = { 0, half_height - 50, cn_nFieldWidth * FIELD_CELL_SIZE, half_height };
		FillRect(hdc, &rect, WHITE_BRUSH);
		DrawText(hdc, L"Pause in game...\0", -1, &rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	}
}


void c_SnakeGame::DoLogic()
{
	if (bIsGameOver || m_bPause) {
		return;
	}

	setSnakeDirection(_snakeNextDirection);
	_snakeNextDirection = snakeCurrentDirection;	//current direction might not change; need to ensure, that _snakeNextDirection contains actual value

	moveSnake();

	auto snake_head = pc_snakeSegments->at(0);

	//check collision with walls in head's position:
	if (ppc_gameArea[snake_head.m_nX][snake_head.m_nY] == en_cellState::eWall) {
		bIsGameOver = true;
		if (cfp_gameOverCallback != nullptr) {
			cfp_gameOverCallback();
		}
	}
	//check collision with tail:
	else if (isSnakeTailSegmentOnCell(snake_head.m_nX, snake_head.m_nY)) {
		//take index of segment with which collision occured:
		int collided_segment_index = getSegmentIndexWithCoordinates(snake_head.m_nX, snake_head.m_nY, false);
		int snake_length_delta = pc_snakeSegments->size() - collided_segment_index;

		//decrease game speed proportionaly:
		_fGameSpeedFactor -= st_cn_fGameSpeedFactorIncreaseDelta * snake_length_delta;
		if (_fGameSpeedFactor < st_cn_fMinGameSpeedFactor) {
			_fGameSpeedFactor = st_cn_fMinGameSpeedFactor;
		}

		//reduce game score:
		_nGameScore -= snake_length_delta;
		if (_nGameScore < 0) {
			_nGameScore = 0;
		}

		truncateTailAfterSegment(collided_segment_index);
	}
	//check collision with targets:
	else if (ppc_gameArea[snake_head.m_nX][snake_head.m_nY] == en_cellState::eTarget) {
		lengthenTail();

		ppc_gameArea[snake_head.m_nX][snake_head.m_nY] = en_cellState::eEmpty;	//remove old target

		generateRandomTarget();

		//speed up the game:
		_fGameSpeedFactor += st_cn_fGameSpeedFactorIncreaseDelta;
		if (_fGameSpeedFactor > st_cn_fMaxGameSpeedFactor) {
			_fGameSpeedFactor = st_cn_fMaxGameSpeedFactor;
		}

		_nGameScore++;
	}
}


void c_SnakeGame::initGameForStart()
{
	initGameArea();

	pc_snakeSegments->clear();
	initSnake(cn_nDesiredSnakeLength);

	snakeCurrentDirection = en_MoveDirections::eDown;
	_snakeNextDirection = snakeCurrentDirection;

	bIsGameOver = false;

	_fGameSpeedFactor = c_SnakeGame::st_cn_fMinGameSpeedFactor;

	_nGameScore = 0;

	generateRandomTarget();
}


void c_SnakeGame::initGameArea()
{
	for (int x = 0; x < cn_nFieldWidth; ++x) {
		for (int y = 0; y < cn_nFieldHeight; ++y) {
			ppc_gameArea[x][y] = en_cellState::eEmpty;
			//border cells are walls:
			if (x == 0 || x == cn_nFieldWidth - 1 || y == 0 || y == cn_nFieldHeight - 1) {
				ppc_gameArea[x][y] = en_cellState::eWall;
			}
		}
	}
}


void c_SnakeGame::generateRandomTarget()
{
	int x, y;

	x = rand() % cn_nFieldWidth;
	y = rand() % cn_nFieldHeight;

	//search a point until we find a free cell within the boundaries of the map:
	while (ppc_gameArea[x][y] != en_cellState::eEmpty || isAnySnakeSegmentOnCell(x, y))
	{
		x = rand() % cn_nFieldWidth;
		y = rand() % cn_nFieldHeight;
	}

	ppc_gameArea[x][y] = en_cellState::eTarget;
}


void c_SnakeGame::initSnake(int length)
{
#if _DEBUG
	assert(length > 0);
#endif
	if (length > cn_nFieldHeight - 5) {
		length = cn_nFieldHeight - 5;
	}

	while (length > 0) {
		pc_snakeSegments->push_back(s_snakeSegment(2, length + 2));
		length--;
	}
}


void c_SnakeGame::moveSnake()
{
	//move tale:
	int length = pc_snakeSegments->size();
	for (int i = length - 1; i >= 1; --i) {
		if (i == length - 1) {
			lastTailPosition.x = (*pc_snakeSegments)[i].m_nX;
			lastTailPosition.y = (*pc_snakeSegments)[i].m_nY;
		}
		(*pc_snakeSegments)[i] = (*pc_snakeSegments)[i - 1];
	}

	s_snakeSegment& head_ref = pc_snakeSegments->at(0);

	//move head:
	switch (snakeCurrentDirection) {
		case en_MoveDirections::eUp:
			head_ref.m_nY--;
			break;
		case en_MoveDirections::eDown:
			head_ref.m_nY++;
			break;
		case en_MoveDirections::eLeft:
			head_ref.m_nX--;
			break;
		case en_MoveDirections::eRight:
			head_ref.m_nX++;
			break;
	}
}


int c_SnakeGame::getSegmentIndexWithCoordinates(int x, int y, bool considerHead)
{
	int length = pc_snakeSegments->size();

	for (int i = (considerHead ? 0 : 1); i < length; ++i) {
		auto& segment_ref = pc_snakeSegments->at(i);
		if (segment_ref.m_nX == x && segment_ref.m_nY == y) {
			return i;
		}
	}
	return -1;
}


bool c_SnakeGame::isAnySnakeSegmentOnCell(int x, int y)
{
	return getSegmentIndexWithCoordinates(x, y, true) != -1;
}


bool c_SnakeGame::isSnakeTailSegmentOnCell(int x, int y)
{
	return getSegmentIndexWithCoordinates(x, y, false) != -1;
}


void c_SnakeGame::truncateTailAfterSegment(int index)
{
#if _DEBUG
	assert(index < ( int )pc_snakeSegments->size());
#endif
	pc_snakeSegments->resize(index + 1);
}


void c_SnakeGame::lengthenTail()
{
	pc_snakeSegments->push_back(s_snakeSegment(lastTailPosition.x, lastTailPosition.y));
}


void c_SnakeGame::setSnakeDirection(en_MoveDirections direction)
{
	//can't set opposite direction:
	if (direction == en_MoveDirections::eUp && snakeCurrentDirection == en_MoveDirections::eDown) {
		return;
	}
	else if (direction == en_MoveDirections::eDown && snakeCurrentDirection == en_MoveDirections::eUp) {
		return;
	}
	else if (direction == en_MoveDirections::eLeft && snakeCurrentDirection == en_MoveDirections::eRight) {
		return;
	}
	else if (direction == en_MoveDirections::eRight && snakeCurrentDirection == en_MoveDirections::eLeft) {
		return;
	}
	snakeCurrentDirection = direction;
}