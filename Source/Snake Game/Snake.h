#pragma once

#include <windows.h>
#include <vector>

#define FIELD_CELL_SIZE 20	//measured in pixels or draw units
#define FIELD_CELL_BORDER_WIDTH 3	//measured in pixels or draw units


enum struct en_MoveDirections : char
{
	eUp    = 0x00,
	eDown  = 0x01,
	eLeft  = 0x02,
	eRight = 0x03
};


typedef void(*fp_GameOverCallback_t)();


class c_SnakeGame
{
public:
	const int cn_nFieldWidth = 20;	//measured in Cells
	const int cn_nFieldHeight = 25;

	bool m_bPause = false;

	c_SnakeGame(int desiredSnakeLength, fp_GameOverCallback_t gameOverCallback);	//desiredLength may be reduced, if necessary
	c_SnakeGame(const c_SnakeGame&) = delete;
	~c_SnakeGame();

	void Set_SnakeNextDirection(en_MoveDirections direction) { _snakeNextDirection = direction; }
	en_MoveDirections Get_SnakeNextDirection() { return _snakeNextDirection; }
	float Get_GameSpeedFactor() { return _fGameSpeedFactor; }
	int Get_GameScore() { return _nGameScore; }

	void Draw(HDC hdc);
	void DoLogic();
	void Restart() { initGameForStart(); }


private:
	enum struct en_cellState : char
	{
		eEmpty  = 0x00,
		eWall	= 0x01,
		eTarget	= 0x02
	};
	struct s_snakeSegment
	{
		int m_nX, m_nY;

		s_snakeSegment() = default;
		s_snakeSegment(int x, int y)
			:m_nX(x), m_nY(y)
		{ /* noop */ }
	};

	static const float st_cn_fMinGameSpeedFactor;
	static const float st_cn_fMaxGameSpeedFactor;
	static const float st_cn_fGameSpeedFactorIncreaseDelta;

	const HBRUSH cn_backgroundBrush;
	const HBRUSH cn_wallBrush;
	const HBRUSH cn_targetBrush;
	en_cellState** const ppc_gameArea;	//defines content of each cell; indexation: [x][y]
	std::vector<s_snakeSegment>* const pc_snakeSegments;	//head at index [0]
	const fp_GameOverCallback_t cfp_gameOverCallback;		//called by DoLogic, when game over happend
	const int cn_nDesiredSnakeLength;

	en_MoveDirections _snakeNextDirection;
	float _fGameSpeedFactor;
	int _nGameScore;

	en_MoveDirections snakeCurrentDirection;	//change via setSnakeDirection
	HPEN snakePen;
	POINT lastTailPosition = { 0, 0 };	//<x, y> filled in moveSnake() function; used to increase snake length
	bool bIsGameOver = false;

	void initGameForStart();	//set values for all non-const variables (except snakePen) and prepare game for start
	/* game area related: */
	void initGameArea();
	/* targets-related: */
	void generateRandomTarget();
	/* snake-related: */
	void initSnake(int length);
	void moveSnake();
	int getSegmentIndexWithCoordinates(int x, int y, bool considerHead);	//if not found, return -1
	bool isAnySnakeSegmentOnCell(int x, int y);
	bool isSnakeTailSegmentOnCell(int x, int y);	//any segment except head consider tail
	void truncateTailAfterSegment(int index);		//remove any snake segment after given index
	void lengthenTail();
	void setSnakeDirection(en_MoveDirections direction);
};